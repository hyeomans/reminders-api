'use strict';
const path = require('path');

module.exports = {
  development: {
    client: 'sqlite3',
    connection: {
      filename: path.resolve('.', 'tmp', 'database.db')
    },
    debug: true,
    //http://knexjs.org/#Builder-insert
    useNullAsDefault: true,
    migrations: {
      tableName: 'migrations'
    }
  }
}