'use strict';
const path          = require('path');
const logger        = require('morgan');
const knex          = require('knex');

const environment   = process.env.NODE_ENV || 'dev';
const knexConfig    = require(path.resolve('.', 'src', 'config', 'database', 'knexfile'));

const config = {
  'dev': {
    database: knexConfig.development
  }
}

module.exports = function() {
  let currentConfig = config[environment.toLowerCase()] || config['dev'];
  return {
    db: knex(currentConfig.database),
    logger: logger.call(null, environment)
  }
}