'use strict';
const port              = process.env.PORT || 4000;

const path              = require('path');
const express           = require('express');
const app               = express();
const bodyParser        = require('body-parser');

const router            = express.Router();

const resolve           = path.resolve;
const routes            = require(resolve('.', 'src', 'routes'))(router);
const allowCrossDomain  = require(resolve('.', 'src', 'middleware', 'cross-domain'));
const config            = require(resolve('.', 'src', 'config'))();

app.use(config.logger);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(allowCrossDomain);

app.use(routes);

app.listen(port, () => console.log('Serving'));

module.exports = app;