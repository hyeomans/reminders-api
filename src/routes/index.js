'use strict';
const path        = require('path');
const v1Path      = path.resolve('.', 'src', 'routes', 'api', 'v1');
const authRoutes  = require(path.resolve(v1Path, 'auth'));

module.exports = index;

function index(router) {
  var withAuthRoutes = authRoutes(router);

  return router;
}