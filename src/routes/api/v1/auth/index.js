'use strict';
const path                = require('path');
const routeHandlersPath   = path.resolve(process.cwd(), 'src', 'routes', 'api', 'v1', 'auth');
const registerHandler     = require(path.resolve(`${routeHandlersPath}`, 'register'));

module.exports = authRoutes;

function authRoutes(router) {
  router.post(registerHandler.path, registerHandler.handler);
  
  return router;
}