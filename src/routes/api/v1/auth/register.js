'use strict';
const path          = require('path');
const servicesPath  = path.resolve(process.cwd(), 'src', 'services');
const services      = require(`${servicesPath}`);

const registerPath  = '/api/v1/auth/register';

module.exports = {
  path: '/api/v1/auth/register',
  handler: register
};

function register(req, res) {
  return services
            .registration(req.body)
            .then((holis) => {
              return res.json({holis: holis});
            });
}