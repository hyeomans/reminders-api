'use strict';
const path          = require('path');
const registration  = require(path.resolve('.', 'src', 'services', 'registration'));

module.exports = {
  registration: registration
}