'use strict';
const path                  = require('path');
const sinon                 = require('sinon');
const chai                  = require('chai');
const expect                = chai.expect;
const crossDomainMiddleware = require(path.resolve('src', 'middleware', 'cross-domain'));

describe('cross-domain test', () => {
  let mockResponse = {
    header: sinon.stub()
  };

  let mockNext     = sinon.spy();

  it('sets the correct headers', () => {
    crossDomainMiddleware(null, mockResponse, mockNext);
    expect(mockResponse.header.getCall(0).args).to.eql([ 'Access-Control-Allow-Origin', '*' ]);
    expect(mockResponse.header.getCall(1).args).to.eql([ 'Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE' ]);
    expect(mockResponse.header.getCall(2).args).to.eql([ 'Access-Control-Allow-Headers', 'Content-Type' ]);
    expect(mockNext.called).to.be.true;
  });
});