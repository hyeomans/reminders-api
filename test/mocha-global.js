'use strict';
const path              = require('path');
const sinon             = require('sinon');
const chai              = require('chai');
const chaisAsPromised   = require("chai-as-promised");
const expect            = chai.expect;
const P                 = require('bluebird');

chai.use(chaisAsPromised);

const srcPath           = path.resolve('.', 'src');

global.srcPath  = srcPath;
global.expect   = expect;
global.sinon    = sinon;
global.P        = P;