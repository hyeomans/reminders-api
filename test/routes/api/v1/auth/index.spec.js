'use strict';
const path           = require('path');
const authRoutesPath = path.resolve(srcPath, 'routes', 'api', 'v1', 'auth');
let authRoutes       = require(authRoutesPath);

const totalAuthRoutes = 1;

describe('Auth routes', () => {
  const expressRouterStub = {
    post: sinon.stub()
  };

  it('should add register routes', () => {
    let result = authRoutes(expressRouterStub);
    let expectedRoute = '/api/v1/auth/register';
    let registerRoute = expressRouterStub.post.getCall(0).args[0];
    expect(registerRoute).to.eql(expectedRoute);
  });

  it('knows when new routes are added', () => {
    let result = authRoutes(expressRouterStub);
    let currentCount = expressRouterStub.post.callCount;
    expect(totalAuthRoutes).to.eql(currentCount);
  });

  afterEach(() => expressRouterStub.post.reset());
});